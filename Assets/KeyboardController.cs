﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class KeyboardController : MonoBehaviour
{
    // private CustomTMP_InputField _input;
    private TMP_InputField _input;
    private bool _keyboardIsVisible;

    void Start()
    {
        _input = GetComponent<TMP_InputField>();
        _keyboardIsVisible = false;
        _guiStyle.fontSize = 26;
        
        
    }

    private TouchScreenKeyboard.Status _keyboardStatus;
    private TouchScreenKeyboard.Status _prevKeyboardStatus;
    private bool _keyboardIsActive;
    private bool _prevKeyboardIsActive;
    
    private GUIStyle _guiStyle = new GUIStyle();
    private int _statusChangedCounter = 0;

    private Coroutine _openCoroutine;
    private TouchScreenKeyboard _keyboard;

    void OnGUI()
    {
        var keyboardText = string.Join("\r\n", _messages);
        
        GUI.Label(new Rect(10, 250, 600, 600), keyboardText, _guiStyle);
        var rectTransform = (RectTransform)_input.transform;
        
        if (GUI.Button(new Rect(10, 50, 200, 100), "Open"))
        {
            _keyboard = TouchScreenKeyboard.Open("asd", TouchScreenKeyboardType.Default);

            if (_openCoroutine != null)
            {
                StopCoroutine(_openCoroutine);
            }

            _timer = 30f;
            _isActive = false;
            _isVisible = false;
            _openCoroutine = StartCoroutine(OpeningWatcher());
        }

        // if (GUI.Button(new Rect(10, 150, 200, 100), "Hide"))
        // {
        //     _keyboardCtrl.HideKeyboard();
        // }
    }

    private bool _isActive = false;
    private bool _isVisible = false;
    private Rect _keyboardArea;
    private float _timer = 15f;
    
    private IEnumerator OpeningWatcher()
    {
        bool isFinished = false;

        while (!isFinished)
        {
            if (_keyboard.active && _isActive == false)
            {
                _isActive = true;
                
                var messageTime = GenerateMessageTime();
                string message = messageTime + $"active: {_isActive}";
                RegisterNewMessage(message);
            }

            if (TouchScreenKeyboard.visible == true && _isVisible == false)
            {
                _isVisible = true;
                
                var messageTime = GenerateMessageTime();
                string message = messageTime + $"visible: {_isVisible}";
                RegisterNewMessage(message);
            }

            if (_isActive && _isVisible)
            {
                // если появилась клава, то SoftKeyboardUtilities.GetKeyboardHeightRatio() возвращает норм значение,
                // примерно == 0.49
                // и 0.736, если экран горизонтально
                yield return new WaitForSeconds(5);
                var messageTime = GenerateMessageTime();
            
                var ratio = SoftKeyboardUtilities.GetKeyboardHeightRatio();
                string message = messageTime + $"ratio: {ratio}";
                RegisterNewMessage(message);
                
                isFinished = true;
            }
            
            // if (_isActive && _isVisible)
            // {
            //     // высота == 498, если подождать, пока появится клава полностью, если не ждать = 0
            //     yield return new WaitForSeconds(5);
            //     var messageTime = GenerateMessageTime();
            //
            //     var ratio = SoftKeyboardUtilities.GetKeyboardSize();
            //     string message = messageTime + $"ratio: {ratio}";
            //     RegisterNewMessage(message);
            //     
            //     isFinished = true;
            // }
            
            // if (_isActive && _isVisible)
            // {
            //     // похоже, кидает исключение
            //     yield return new WaitForSeconds(5);
            //     var messageTime = GenerateMessageTime();
            //
            //     var ratio = MobileUtilities.GetKeyboardHeight1(true);
            //     string message = messageTime + $"ratio: {ratio}";
            //     RegisterNewMessage(message);
            //     
            //     isFinished = true;
            // }
            
            // if (_isActive && _isVisible)
            // {
            //     // похоже, тоже кидает исключение
            //     yield return new WaitForSeconds(5);
            //     var messageTime = GenerateMessageTime();
            //
            //     var ratio = MobileUtilities.GetKeyboardHeight(true);
            //     string message = messageTime + $"ratio: {ratio}";
            //     RegisterNewMessage(message);
            //     
            //     isFinished = true;
            // }

            _timer -= Time.deltaTime * Time.timeScale;
            if (_timer <= 0)
            {
                // работает, выдало 498 и 400 при вертикальной и горизонтальной анимации
                var messageTime = GenerateMessageTime();
                string message = messageTime + $"finished by timeout";
                RegisterNewMessage(message);
                
                isFinished = true;
            }

            yield return null;
        }
    }


    public void ShowKeyboard()
    {
        _keyboardIsVisible = true;
        _input.ActivateInputField();
    }

    public void HideKeyboard()
    {
        _keyboardIsVisible = false;
        _input.DeactivateInputField(true);
    }

    private bool _touchScreenKeyboard = false;
    private List<string> _messages = new List<string>()
    {
        "roman1: 1s:11ms, true", 
        "roman2: 1s:11ms, true", 
        "roman3: 1s:11ms, true", 
        "roman4: 1s:11ms, true", 
        "roman5: 1s:11ms, true", 
        "roman6: 1s:11ms, true", 
    };

    private void RegisterNewMessage(string message)
    {
        _messages.RemoveAt(0);
        _messages.Add(message);
    }

    private string GenerateMessageTime()
    {
        var time = DateTime.Now;
        var seconds = time.Second;
        var mSeconds = time.Millisecond;
        var result = $"{seconds}s:{mSeconds}ms, ";
        return result;
    }

    void Update()
    {
        bool visible = TouchScreenKeyboard.visible;
    
        if (visible != _touchScreenKeyboard)
        {
            var heightWithInput = MobileUtilities.GetKeyboardHeight(true);
            // var heightWithoutInput = MobileUtilities.GetKeyboardHeight(false);
            //var heightWithoutInput = MobileUtilities.GetKeyboardHeightRatio();
            // var height = Cham.SoftwareKeyboaryArea.Height;

            var messageTime = GenerateMessageTime();
            
            // string message = $"roman: {seconds}s:{mSeconds}ms, {visible}, with: {heightWithInput} without: {heightWithoutInput}";
            string message = messageTime + $"{visible}, with: {heightWithInput}";
            
            RegisterNewMessage(message);
    
            _touchScreenKeyboard = visible;
        }
    }

    // void Update()
    // {
    //     TouchScreenKeyboard.area
    //     TouchScreenKeyboard keyboard = TouchScreenKeyboard.Open();
    //     
    //     Touch touch = Input.GetTouch(0);
    //     if (touch.phase == TouchPhase.Began)
    //     {
    //         Vector2 touchPos = touch.position;
    //         Ray ray = Camera.main.ScreenPointToRay(touchPos);
    //         RaycastHit[] castResult = Physics.RaycastAll(ray, Mathf.Infinity, ~0);
    //
    //         for (int i = 0; i < castResult.Length; i++)
    //         {
    //             RaycastHit hit = castResult[i];
    //             
    //         }
    //     }
    // }

}
