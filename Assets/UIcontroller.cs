﻿using UnityEngine;

public class UIcontroller : MonoBehaviour
{
    public KeyboardController _keyboardCtrl;

    void Start()
    {
        
    }

    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 50, 200, 100), "Open"))
        {
            _keyboardCtrl.ShowKeyboard();
        }
        
        if (GUI.Button(new Rect(10, 150, 200, 100), "Hide"))
        {
            _keyboardCtrl.HideKeyboard();
        }

    }

}
