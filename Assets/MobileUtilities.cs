﻿using UnityEngine;


public static class SoftKeyboardUtilities
{
    public static float GetKeyboardHeightRatio()
    {
        if (Application.isEditor)
            return 0.4f;
   
#if UNITY_ANDROID
        using (var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            var view = unityClass.GetStatic<AndroidJavaObject>("currentActivity")
                .Get<AndroidJavaObject>("mUnityPlayer")
                .Call<AndroidJavaObject>("getView");
       
            var dialog = unityClass.GetStatic<AndroidJavaObject>("currentActivity")
                .Get<AndroidJavaObject>("mUnityPlayer")
                .Get<AndroidJavaObject>("b");
       
            var decorView = dialog.Call<AndroidJavaObject>("getWindow")
                .Call<AndroidJavaObject>("getDecorView");
       
            var height = decorView.Call<int>("getHeight");
       
            using (var rect = new AndroidJavaObject("android.graphics.Rect"))
            {
                view.Call("getWindowVisibleDisplayFrame", rect);
                return (float) (Screen.height - rect.Call<int>("height") + height) / Screen.height;
            }
        }
#elif UNITY_IOS
        return (float) TouchScreenKeyboard.area.height / Screen.height;
#endif
    }

    
    public static int GetKeyboardSize()
    {
        using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");
            using (AndroidJavaObject Rct = new AndroidJavaObject("android.graphics.Rect"))
            {
                View.Call("getWindowVisibleDisplayFrame", Rct);
                return Screen.height - Rct.Call<int>("height");
            }
        }
    }
}


//  этот код не работает
public static class MobileUtilities
{
    /// <summary>
    /// Returns the keyboard height ratio.GetKeyboardSize
    /// </summary>
    public static float GetKeyboardHeightRatio(bool includeInput)
    {
        return Mathf.Clamp01((float) GetKeyboardHeight1(includeInput) / Display.main.systemHeight);
    }
 
    /// <summary>
    /// Returns the keyboard height in display pixels.
    /// </summary>
    public static int GetKeyboardHeight1(bool includeInput)
    {
#if UNITY_ANDROID
        using (var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            var unityPlayer = unityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer");
            var view = unityPlayer.Call<AndroidJavaObject>("getView");
            var dialog = unityPlayer.Get<AndroidJavaObject>("mSoftInputDialog");
 
            if (view == null || dialog == null)
                return 0;
 
            var decorHeight = 0;
 
            if (includeInput)
            {
                var decorView = dialog.Call<AndroidJavaObject>("getWindow").Call<AndroidJavaObject>("getDecorView");
 
                if (decorView != null)
                    decorHeight = decorView.Call<int>("getHeight");
            }
 
            using (var rect = new AndroidJavaObject("android.graphics.Rect"))
            {
                view.Call("getWindowVisibleDisplayFrame", rect);
                return Display.main.systemHeight - rect.Call<int>("height") + decorHeight;
            }
        }
#else
        var height = Mathf.RoundToInt(TouchScreenKeyboard.area.height);
        return height >= Display.main.systemHeight ? 0 : height;
#endif
    }
    
    public static int GetKeyboardHeight(bool includeInput)
    {
#if UNITY_ANDROID
        using (var unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
            var unityPlayer = unityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer");
            var view = unityPlayer.Call<AndroidJavaObject>("getView");
 
            var result = 0;
               
            if (view != null) {
                using (var rect = new AndroidJavaObject("android.graphics.Rect"))
                {
                    view.Call("getWindowVisibleDisplayFrame", rect);
                    result = Display.main.systemHeight - rect.Call<int>("height");
                }
 
                if (includeInput) {
                    var dialog = unityPlayer.Get<AndroidJavaObject>("mSoftInputDialog");
                    var decorView = dialog?.Call<AndroidJavaObject>("getWindow").Call<AndroidJavaObject>("getDecorView");
 
                    if (decorView != null) {
                        var decorHeight = decorView.Call<int>("getHeight");
                        result += decorHeight;
                    }
                }
            }
               
            return result;
        }
#else
            var height = Mathf.RoundToInt(TouchScreenKeyboard.area.height);
            return height >= Display.main.systemHeight ? 0 : height;
#endif
    }
}

namespace Cham
{
    /// <summary>
    /// ソフトウェアキーボードの表示領域を管理するクラス
    /// </summary>
    public static class SoftwareKeyboaryArea
    {
        /// <summary>
        /// 高さを返します
        /// </summary>
        public static int Height
        {
            get
            {
#if !UNITY_EDITOR && UNITY_ANDROID

                using ( var unityPlayer = new AndroidJavaClass( "com.unity3d.player.UnityPlayer" ) )
                {
                    var view = unityPlayer
                        .GetStatic<AndroidJavaObject>( "currentActivity" )
                        .Get<AndroidJavaObject>( "mUnityPlayer" )
                        .Call<AndroidJavaObject>( "getView" )
                    ;

                    using ( var rect = new AndroidJavaObject( "android.graphics.Rect" ) )
                    {
                        view.Call( "getWindowVisibleDisplayFrame", rect );

                        return Screen.height - rect.Call<int>( "height" );
                    }
                }
#else
                return ( int )TouchScreenKeyboard.area.height;
#endif
            }
        }
    }
}
